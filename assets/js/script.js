window.page = 1
window.size = 10
window.sort = "published_at"

$(document).ready(function () {
    function dateToStr(tanggalString) {
        var tanggalObjek = new Date(tanggalString);
        var namaBulan = [
            "Januari", "Februari", "Maret",
            "April", "Mei", "Juni",
            "Juli", "Agustus", "September",
            "Oktober", "November", "Desember"
        ];
        var tanggal = tanggalObjek.getDate();
        var bulan = namaBulan[tanggalObjek.getMonth()];
        var tahun = tanggalObjek.getFullYear();
        var hasilFormat = tanggal + " " + bulan + " " + tahun;
        return hasilFormat
    }

    function renderPagination(data) {
        const page_view = 5
        const links = data.links
        const meta = data.meta
        const total_page = meta.last_page
        $('.pagination').html('')
        $('.pagination').append(`<li class="page-item ${links.prev ? '' : 'disabled'}"><a class="page-link" href="#" data-page="first"><i class="fa-solid fa-chevrons-left"></i></a></li>`)
        $('.pagination').append(`<li class="page-item ${links.prev ? '' : 'disabled'}"><a class="page-link" href="#" data-page="${meta.current_page - 1}"><i class="fa-solid fa-chevron-left"></i></a></li>`)
        $('.pagination').append(`<li class="page-item active" aria-current="page"><span class="page-link">${meta.current_page}</span></li>`)
        for (let i = meta.current_page + 1; i < (meta.current_page + page_view); i++) {
            if (i <= total_page) {
                $('.pagination').append(`<li class="page-item"><a class="page-link" href="#page" data-page="${i}">${i}</a></li>`)
            }
        }
        $('.pagination').append(`<li class="page-item ${(links.next) ? '' : 'disabled'}"><a class="page-link" href="#" data-page="${meta.current_page + page_view}"><i class="fa-solid fa-chevron-right"></i></a></li>`)
        $('.pagination').append(`<li class="page-item ${(links.next) ? '' : 'disabled'}"><a class="page-link" href="#" data-page="last"><i class="fa-solid fa-chevrons-right"></i></a></li>`)

        $('.page-link').click(function () {
            const page_to = $(this).data('page');
            console.log(page_to)
            if (page_to == 'first') {
                page = 1
                renderNews(page, size, sort)
            } else if (page_to == 'last') {
                page = total_page
                renderNews(page, size, sort)
            } else {
                page = page_to
                renderNews(page, size, sort)
            }
        })
    }

    function renderNews(page, size, sort) {
        const placeholder_image = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTbV5yDFnmskE8YIeejdmgr3dX_Vz9CVmlczA6WZ_ZoQnWuYqiNlrWhLdF9jFrqtwqAQwY&usqp=CAU"
        $('.news-content').html('')
        $.ajax({
            url: `https://suitmedia-backend.suitdev.com/api/ideas?page[number]=${page}&page[size]=${size}&append[]=small_image&append[]=medium_image&sort=${sort}`,
            headers: { "Accept": "application/json" },
            success: (data) => {
                console.log(data);
                renderPagination(data)
                const { from, to, total, current_page, last_page, links } = data.meta
                $('#first_num').text(from)
                $('#last_num').text(to)
                $('#total_num').text(total)
                for (const news of data.data) {
                    $('.news-content').append(`
                    <div class="col">
                        <div class="card news-item">
                            <img src="${news.medium_image[0]?.url ?? placeholder_image}" class="card-img-top" alt="${news.slug}">
                            <div class="card-body">
                                <p class="text-muted">${dateToStr(news.published_at)}</p>
                                <h5 class="news-title">${news.title}</h5>
                            </div>
                        </div>
                    </div>
                    `)
                }
            }
        })
    }

    $('select').on('change', function () {
        const el = $(this)
        if (el.attr('name') == 'page_filter') window.size = el.val()
        if (el.attr('name') == 'sort_filter') window.sort = el.val()
        renderNews(page, size, sort)
    })


    renderNews(page, size, sort)
})

const header = document.querySelector('.navbar');
let scrollPos = 0;

window.addEventListener('scroll', () => {
    const currentScrollPos = window.pageYOffset;
    if (currentScrollPos == 0){
        header.classList.remove('scrolledDown');
        header.classList.remove('scrolledUp');
    }
    else if (currentScrollPos < scrollPos) {
        header.classList.add('scrolledDown');
        header.classList.remove('scrolledUp');
    } else {
        header.classList.remove('scrolledDown');
        header.classList.add('scrolledUp');
    }

    scrollPos = currentScrollPos;
});

var scene = document.getElementsByClassName('imageBanner');
var parallaxInstance = new Parallax(scene);